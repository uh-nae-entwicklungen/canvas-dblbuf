# canvas-dblbuf

Helper project for Letters Rain - examine behaviour of canvas fading and watch performance/load

# WARNING

Flashing patterns displayed at the html page canvas-dblbuf.html could
trigger photosensitive epilepsy (PSE)!